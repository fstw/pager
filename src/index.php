<?php

require_once("../vendor/autoload.php");
require_once("./pager.php");

$loader = new \Twig_Loader_Filesystem(__DIR__ . '/views');
$twig = new \Twig_Environment($loader);

$pager = new Pager(
    'http://localhost:8000/src/index.php?q=foobar',
    1,
    10,
    8,
    2000
);

$start = ($pager->getCurrentPage() <= $pager->range) ? 1 : ($pager->getCurrentPage() - $pager->range);
$end = ($pager->totalPages - $pager->getCurrentPage() >= $pager->range) ? ($pager->getCurrentPage() + $pager->range) : $pager->totalPages;

echo $twig->render('pagination.html.twig', ['pager' => $pager, 'start' => $start, 'end' => $end]);
