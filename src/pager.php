<?php


require_once '../vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Pager
{
    protected $logger;

    public function __construct($link, $currentPage, $itemPerPage, $pageWidth, $totalItems)
    {
        $this->link = $link;
        $this->currentPage = $currentPage;
        $this->itemPerPage = $itemPerPage;
        $this->pagerWidth = $pageWidth;
        $this->pagerWidth2 = floor($pageWidth / 2);
        $this->totalItems = $totalItems;
        $this->totalPages = ceil($totalItems / $itemPerPage);

        $this->range = 5;

        $this->logger = new Logger('tpl');
        $this->logger->pushHandler(new StreamHandler('../logs/tpl.log', Logger::DEBUG));
    }



    public function hasPrev()
    {
        return $this->getCurrentPage() > 1;
    }
    public function hasNext()
    {
        return  $this->getCurrentPage() < $this->getEndPage();
    }
    public function getLink()
    {
        return $this->link;
    }

    public function getCurrentPage()
    {
        return isset($_GET['page']) ? $_GET['page'] : 1;
    }

    public function getStartPage()
    {
        return 1;
    }

    public function getEndPage()
    {
        return $this->totalPages;
    }
}
