# pager

## run locally

git clone

cd pager

`./composer.phar start`

## test

make sure the app is running on localhost:8000

download phnatom.js and run it like so "bin/phantomjs --webdriver=4444"

from the pager directory type `./composer.phar test`
