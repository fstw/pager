<?php

class PagerCest
{
    public function _before(AcceptanceTester $I)
    { }

    public function hasPrevTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->dontsee('<');
        $I->click('4');
        $I->see('<');
        $I->see('1');
    }

    public function hasNextTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('>');
        $I->click('>');
        $I->see('5');
    }

    public function getCurrentPage(AcceptanceTester $I)
    { }

    public function getStartTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('>>');
        $I->click('>>');
        $I->see('200');
        $I->dontsee('>>');
    }
    public function getEndTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->dontsee('<<');
        $I->click('>>');
        $I->dontsee('>>');
        $I->see('<<');
        $I->see('1');
    }
}
